/* Copyright (c) 2010 - 2020, Nordic Semiconductor ASA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <string.h>

/* HAL */
#include "boards.h"
#include "app_timer.h"
#include "nrf_delay.h"

/* Core */
#include "nrf_mesh_config_core.h"
#include "nrf_mesh_gatt.h"
#include "nrf_mesh_configure.h"
#include "nrf_mesh.h"
#include "nrf_mesh_events.h"
#include "nrf_mesh_assert.h"
#include "flash_manager.h"
#include "mesh_stack.h"
#include "device_state_manager.h"
#include "access_config.h"
#include "proxy.h"

/* Provisioning and configuration */
#include "mesh_provisionee.h"
#include "mesh_app_utils.h"

/* Models */
#include "generic_onoff_server.h"
#include "generic_level_server.h"

/* Logging and RTT */
#include "log.h"
#include "rtt_input.h"

/* Example specific includes */
#include "nrf_mesh_config_app.h"
#include "nrf.h"
#include "app_config.h"
#include "example_common.h"
#include "pwm_utils.h"
#include "nrf_mesh_config_examples.h"
#include "app_level.h"
#include "app_onoff.h"
#include "ble_softdevice_support.h"
#include "ad_type_filter.h"

#include "enocean_switch_example.h"
#include "enocean.h"
//#include "access_reliable.h"

#include "app_scheduler.h"
/*****************************************************************************
 * Definitions
 *****************************************************************************/
#define APP_LEVEL_STEP_SIZE     (2048L)

#define APP_ONOFF_ELEMENT_INDEX     (0)
#define APP_LEVEL_ELEMENT_INDEX     (0)

/* Controls if the model instance should force all mesh messages to be segmented messages. */
#define APP_FORCE_SEGMENTATION  (false)
/* Controls the MIC size used by the model instance for sending the mesh messages. */
#define APP_MIC_SIZE            (NRF_MESH_TRANSMIC_SIZE_SMALL)

/* Configure switch debounce timeout for EnOcean switch here */
#define SWITCH_DEBOUNCE_INTERVAL_US (MS_TO_US(50))
/* Two EnOcean switches can be used in parallel */
#define MAX_ENOCEAN_DEVICES_SUPPORTED (2)

#define APP_STATE_OFF                 (0)
#define APP_STATE_ON                  (1)

#define APP_UNACK_MSG_REPEAT_COUNT    (2)

#define APP_ONOFF_DELAY_MS           (50)
/* Transition time value used by the OnOff client for sending OnOff Set messages. */
#define APP_ONOFF_TRANSITION_TIME_MS (100)

#define MIN_DIMMING_LEVEL (INT16_MIN+2048)

#define APP_SCHED_EVENT_SIZE        MAX(sizeof(uint8_t), APP_TIMER_SCHED_EVENT_DATA_SIZE)
#define APP_SCHED_QUEUE_SIZE        (50)

/*****************************************************************************
 * Forward declaration of static functions
 *****************************************************************************/
static void app_onoff_server_set_cb(const app_onoff_server_t * p_server, bool onoff);
static void app_onoff_server_get_cb(const app_onoff_server_t * p_server, bool * p_present_onoff);
static void app_onoff_server_transition_cb(const app_onoff_server_t * p_server,
                                                uint32_t transition_time_ms, bool target_onoff);

static void app_level_server_set_cb(const app_level_server_t * p_server, int16_t present_level);
static void app_level_server_get_cb(const app_level_server_t * p_server, int16_t * p_present_level);
static void app_level_server_transition_cb(const app_level_server_t * p_server,
                                                uint32_t transition_time_ms, uint16_t target_level,
                                                app_transition_type_t transition_type);

static uint32_t enocean_switch_setter(mesh_config_entry_id_t id, const void * p_entry);
static void enocean_switch_getter(mesh_config_entry_id_t id, void * p_entry);
static void enocean_switch_deleter(mesh_config_entry_id_t id);

static void app_mesh_core_event_cb (const nrf_mesh_evt_t * p_evt);
static void app_start(void);
/*****************************************************************************
 * Static variables
 *****************************************************************************/
static bool m_device_provisioned;

/* Application variable for holding instantaneous level value */
static int32_t m_pwm0_present_level;
static bool led_state_on_off;

/* Application level generic level server structure definition and initialization */
APP_LEVEL_SERVER_DEF(m_level_server_0,
                     APP_FORCE_SEGMENTATION,
                     APP_MIC_SIZE,
                     NULL,
                     app_level_server_set_cb,
                     app_level_server_get_cb,
                     app_level_server_transition_cb);

/* PWM hardware instance and associated variables */
/* Note: PWM cycle period determines the the max value that can be used to represent 100%
 * duty cycles, therefore present_level value scaling is required to get pwm tick value
 * between 0 and pwm_utils_contex_t:pwm_ticks_max.
 */
static APP_PWM_INSTANCE(PWM0, 1);
static app_pwm_config_t m_pwm0_config = APP_PWM_DEFAULT_CONFIG_1CH(200, BSP_LED_0);
static pwm_utils_contex_t m_pwm = {
                                    .p_pwm = &PWM0,
                                    .p_pwm_config = &m_pwm0_config,
                                    .channel = 0
                                  };



/************************* LED CONTROL **********************************/

void led_level_set(int16_t level) {

    if (level < MIN_DIMMING_LEVEL) {
        level = MIN_DIMMING_LEVEL;
    }

    m_pwm0_present_level = level;

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "SET: Level: %d\n", m_pwm0_present_level);

    if (led_state_on_off) {
        pwm_utils_level_set(&m_pwm, m_pwm0_present_level);
    }
}

void led_state_set(bool is_on, bool auto_correction) {

    bool prev_state_on = led_state_on_off;

    led_state_on_off = is_on;

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "SET: state: %d\n", is_on);

    if (led_state_on_off) {
        pwm_utils_enable(&m_pwm);

        if (auto_correction && prev_state_on) {
            m_pwm0_present_level = INT16_MAX;
        }

        led_level_set((int16_t)m_pwm0_present_level);

    } else {
        pwm_utils_disable(&m_pwm);
    }

}


/* Callback for updating the hardware state */
static void app_level_server_set_cb(const app_level_server_t * p_server, int16_t present_level)
{

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "SET1: Level: %d  uint16 %d \n",present_level, (uint16_t)(present_level - INT16_MIN));

    int16_t level;
    if (present_level >= 0) {
        level = present_level + INT16_MIN;
    } else {
        level = present_level - INT16_MIN;
    }
    
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "SET2: Level: %d  uint16 %d \n",level, (uint16_t)(level - INT16_MIN));

    /* Resolve the server instance here if required, this example uses only 1 instance. */

    led_level_set(level);
}

/* Callback for reading the hardware state */
static void app_level_server_get_cb(const app_level_server_t * p_server, int16_t * p_present_level)
{
    /* Resolve the server instance here if required, this example uses only 1 instance. */
    int16_t level;
    if (m_pwm0_present_level > 0) {
        level = m_pwm0_present_level + INT16_MIN;
    } else {
        level = m_pwm0_present_level - INT16_MIN;
    }

    *p_present_level = level;
}

/* Callback for updateing according to transition time. */
static void app_level_server_transition_cb(const app_level_server_t * p_server,
                                                uint32_t transition_time_ms, uint16_t target_level,
                                                app_transition_type_t transition_type)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Transition time: %d, Target level: %d, Transition type: %d\n",
                                       transition_time_ms, target_level, transition_type);
}

/* Generic OnOff server structure definition and initialization */
APP_ONOFF_SERVER_DEF(m_onoff_server_0,
                     APP_FORCE_SEGMENTATION,
                     APP_MIC_SIZE,
                     app_onoff_server_set_cb,
                     app_onoff_server_get_cb,
                     app_onoff_server_transition_cb)

/* Callback for updating the hardware state */
static void app_onoff_server_set_cb(const app_onoff_server_t * p_server, bool onoff)
{
    /* Resolve the server instance here if required, this example uses only 1 instance. */

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Setting GPIO value: %d\n", onoff)

      led_state_set(onoff, false);
}

/* Callback for reading the hardware state */
static void app_onoff_server_get_cb(const app_onoff_server_t * p_server, bool * p_present_onoff)
{
    /* Resolve the server instance here if required, this example uses only 1 instance. */

    *p_present_onoff = led_state_on_off;
}

/* Callback for updating the hardware state */
static void app_onoff_server_transition_cb(const app_onoff_server_t * p_server,
                                                uint32_t transition_time_ms, bool target_onoff)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Transition time: %d, Target OnOff: %d\n",
                                       transition_time_ms, target_onoff);
}

static void app_model_init(void)
{
    /* Instantiate onoff server on element index APP_ONOFF_ELEMENT_INDEX */
    ERROR_CHECK(app_onoff_init(&m_onoff_server_0, APP_ONOFF_ELEMENT_INDEX));
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "App OnOff Model Handle: %d\n", m_onoff_server_0.server.model_handle);

    /* Instantiate level server on element index 0 */
    ERROR_CHECK(app_level_init(&m_level_server_0, APP_LEVEL_ELEMENT_INDEX));
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "App Level Model Handle: %d\n", m_level_server_0.server.model_handle);
}


typedef struct
{
    uint32_t a0_ts;
    uint32_t a1_ts;
    uint32_t b0_ts;
    uint32_t b1_ts;
} app_switch_debounce_state_t;

/* Single advertiser instance. May periodically transmit one packet at a time. */
//static generic_onoff_client_t m_clients[LIGHT_SWITCH_CLIENTS];
static bool m_device_provisioned;

static app_secmat_flash_t m_app_secmat_flash[MAX_ENOCEAN_DEVICES_SUPPORTED];
static enocean_commissioning_secmat_t m_app_secmat[MAX_ENOCEAN_DEVICES_SUPPORTED];
static uint8_t  m_enocean_dev_cnt;
static app_switch_debounce_state_t m_switch_state[MAX_ENOCEAN_DEVICES_SUPPORTED];

static nrf_mesh_evt_handler_t m_mesh_core_event_handler = { .evt_cb = app_mesh_core_event_cb };

static uint8_t enocean_device_index_get(enocean_evt_t * p_evt)
{
    uint8_t i;
    for (i = 0; i < m_enocean_dev_cnt; i++)
    {
        if (memcmp(p_evt->p_ble_gap_addr, m_app_secmat[i].p_ble_gap_addr, BLE_GAP_ADDR_LEN) == 0)
        {
            break;
        }
    }

    /* This should never assert. */
    if (i == m_enocean_dev_cnt)
    {
        APP_ERROR_CHECK(false);
    }

    return i;
}

/* Forward the ADV packets to the Enocean module */
static void rx_callback(const nrf_mesh_adv_packet_rx_data_t * p_rx_data)
{
    enocean_packet_process(p_rx_data);
}

uint8_t dim_tick_count = 0;
uint32_t last_dim_update_time = 0;
uint32_t last_switch_dimming_update_time;
bool dimming_was_started;

uint32_t a0_ch_ts_on;
uint32_t a1_ch_ts_on;
uint32_t b0_ch_ts_on;
uint32_t b1_ch_ts_on;

static bool light_was_off_by_switch;


static void app_switch_debounce(enocean_switch_status_t * p_status, uint8_t index)
{
    uint32_t timestamp = timer_now();
    uint32_t status = NRF_ERROR_INTERNAL;
    model_transition_t transition_params;
    static uint8_t tid = 0;


    transition_params.delay_ms = APP_ONOFF_DELAY_MS;
    transition_params.transition_time_ms = APP_ONOFF_TRANSITION_TIME_MS;

        if (p_status->a0 && timer_diff(timestamp, m_switch_state[index].a0_ts) > SWITCH_DEBOUNCE_INTERVAL_US)
        {
            m_switch_state[index].a0_ts = timestamp;
            if (p_status->action == PRESS_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch a0 pressed\n");
                a0_ch_ts_on = timer_now();
                dim_tick_count = 0;
                last_dim_update_time = 0;
            } else if (p_status->action == RELEASE_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch a0 released\n");
                a0_ch_ts_on = 0;
                if (last_switch_dimming_update_time == 0) {
                    led_state_set(true, true);
                }
                last_switch_dimming_update_time = 0;

            }

        }
        else if (p_status->a1 && timer_diff(timestamp, m_switch_state[index].a1_ts) > SWITCH_DEBOUNCE_INTERVAL_US)
        {
            m_switch_state[index].a1_ts = timestamp;
            
            light_was_off_by_switch = false;
            if (p_status->action == PRESS_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch a1 pressed\n");
                a1_ch_ts_on = timer_now();
                dim_tick_count = 0;
                last_dim_update_time = 0;

            } else if (p_status->action == RELEASE_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch a1 released\n");

                if (a1_ch_ts_on == 0 && led_state_on_off && last_switch_dimming_update_time == 0) {
                    light_was_off_by_switch = true;
                }

                a1_ch_ts_on = 0;
                if (last_switch_dimming_update_time == 0) {

                    led_state_set(false, false);

                }
                last_switch_dimming_update_time = 0;

            }
        }

        status = NRF_ERROR_INTERNAL;

        /* Change state on the nodes subscribed to the Odd group address using 2nd on/off client */
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "timer_diff: %d\n", timer_diff(timestamp, m_switch_state[index].b0_ts));
        if (p_status->b0 && timer_diff(timestamp, m_switch_state[index].b0_ts) > SWITCH_DEBOUNCE_INTERVAL_US)
        {
            m_switch_state[index].b0_ts = timestamp;

            if (p_status->action == PRESS_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch b0 pressed\n");
                b0_ch_ts_on = timer_now();
                dim_tick_count = 0;
                last_dim_update_time = 0;
            } else if (p_status->action == RELEASE_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch b0 released\n");
                b0_ch_ts_on = 0;
                if (last_switch_dimming_update_time == 0) {
                    led_state_set(true, true);

                } else {

                }
                last_switch_dimming_update_time = 0;
            }
        }
        else if (p_status->b1 && timer_diff(timestamp, m_switch_state[index].b1_ts) > SWITCH_DEBOUNCE_INTERVAL_US)
        {
            m_switch_state[index].b1_ts = timestamp;

            light_was_off_by_switch = false;
            if (p_status->action == PRESS_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch b1 pressed\n");
                b1_ch_ts_on = timer_now();
                dim_tick_count = 0;
                last_dim_update_time = 0;
            } else if (p_status->action == RELEASE_ACTION) {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ch b1 released\n");

                if (b1_ch_ts_on == 0 && led_state_on_off && last_switch_dimming_update_time == 0) {
                    light_was_off_by_switch = true;
                }

                b1_ch_ts_on = 0;
                if (last_switch_dimming_update_time == 0) {
                    led_state_set(false, false);
                }
                last_switch_dimming_update_time = 0;

            }
        }
}

/* This example translates the messages from the PTM215B switches to on/off client model messages.
The mapping of the switches and corresponding client model messages is as follows:

Pressing Switch 1 will turn ON LED 1 on the servers with ODD addresses.
Pressing Switch 2 will turn OFF LED 1 on the servers with ODD addresses.
Pressing Switch 3 will turn ON LED 1 on the servers with EVEN addresses.
Pressing Switch 4 will turn OFF LED 1 on the servers with EVEN addresses.
*/
static void app_enocean_cb(enocean_evt_t * p_evt)
{
    if  (p_evt->type == ENOCEAN_EVT_DATA_RECEIVED)
    {
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Sw data A0: %d A1: %d B0: %d B1: %d Action: %d\n",
              p_evt->params.data.status.a0,p_evt->params.data.status.a1,p_evt->params.data.status.b0,
              p_evt->params.data.status.b1,p_evt->params.data.status.action);

        app_switch_debounce(&p_evt->params.data.status, enocean_device_index_get(p_evt));
    }
    else if (p_evt->type == ENOCEAN_EVT_SECURITY_MATERIAL_RECEIVED)
    {

    }
}


#define SWITCH_DIMMING_INTERVAL_MS (MS_TO_US(125))
#define SWITCH_DIMMING_DELAY_INTERVAL_MS (MS_TO_US(700))

#define SWITCH_DIMMING_DELTA 1024


static void light_level_up() {
        //turn on light if it is off
        if (!led_state_on_off) {
            led_state_set(true, false);
        }

        if (m_pwm0_present_level == INT16_MAX) {
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "level is max\n");
            return;
        }

        int16_t level = (m_pwm0_present_level + SWITCH_DIMMING_DELTA) >= INT16_MAX ?
                                   INT16_MAX : m_pwm0_present_level + SWITCH_DIMMING_DELTA;


        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "increase level\n");

        led_level_set(level);
}

static void light_level_down() {
        if (m_pwm0_present_level == MIN_DIMMING_LEVEL) {
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "level is min\n");
            return;
        }

        int16_t level = (m_pwm0_present_level - SWITCH_DIMMING_DELTA) <= MIN_DIMMING_LEVEL ?
                                   MIN_DIMMING_LEVEL : m_pwm0_present_level - SWITCH_DIMMING_DELTA;

        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "decrease level\n");

        led_level_set(level);
}



static void check_switch_dimming_state() {
    
    uint32_t now = timer_now();

    if (last_switch_dimming_update_time != 0 && timer_diff(now, last_switch_dimming_update_time) < SWITCH_DIMMING_INTERVAL_MS) {
        return;
    }

    if (a0_ch_ts_on != 0 && timer_diff(now, a0_ch_ts_on) > SWITCH_DIMMING_DELAY_INTERVAL_MS) {
        last_switch_dimming_update_time = now;
        light_level_up();
        last_dim_update_time = now;
        dim_tick_count++;
    }

    if (a1_ch_ts_on != 0 && timer_diff(now, a1_ch_ts_on) > SWITCH_DIMMING_DELAY_INTERVAL_MS) {
        last_switch_dimming_update_time = now;
        light_level_down();
        last_dim_update_time = now;
        dim_tick_count++;
    }

    if (b0_ch_ts_on != 0 && timer_diff(now, b0_ch_ts_on) > SWITCH_DIMMING_DELAY_INTERVAL_MS) {
        last_switch_dimming_update_time = now;
        light_level_up();
        last_dim_update_time = now;
        dim_tick_count++;
    }

    if (b1_ch_ts_on != 0 && timer_diff(now, b1_ch_ts_on) > SWITCH_DIMMING_DELAY_INTERVAL_MS) {
        last_switch_dimming_update_time = now;
        light_level_down();
        last_dim_update_time = now;
        dim_tick_count++;
    }
}



/*************************************************************************************************/

static void node_reset(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "----- Node reset  -----\n");
    /* This function may return if there are ongoing flash operations. */
    mesh_stack_device_reset();
}

static void config_server_evt_cb(const config_server_evt_t * p_evt)
{
    if (p_evt->type == CONFIG_SERVER_EVT_NODE_RESET)
    {
        /* Trigger clearing of application data and schedule node reset. */
        node_reset();
    }
}

#if NRF_MESH_LOG_ENABLE
static const char m_usage_string[] =
    "\n"
    "\t\t-------------------------------------------------------------\n"
    "\t\t RTT 1) The brightness of the LED 1 decresses in large steps.\n"
    "\t\t RTT 2) The brightness of the LED 1 incresses in large steps.\n"
    "\t\t RTT 4) Clear all the states to reset the node.\n"
    "\t\t-------------------------------------------------------------\n";
#endif

static void button_event_handler(uint32_t button_number)
{
    /* Increase button number because the buttons on the board is marked with 1 to 4 */
    button_number++;
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Button %u pressed\n", button_number);
    switch (button_number)
    {
        /* Sending value `1` or `2` via RTT will result in LED state to change and trigger the
        STATUS message to inform client about the state change. This is a demonstration of state
        change publication due to local event. */
        case 1:
        {
            m_pwm0_present_level = (m_pwm0_present_level - APP_LEVEL_STEP_SIZE) <= INT16_MIN ?
                                   INT16_MIN : m_pwm0_present_level - APP_LEVEL_STEP_SIZE;
            break;
        }

        case 2:
        {
            m_pwm0_present_level = (m_pwm0_present_level + APP_LEVEL_STEP_SIZE) >= INT16_MAX ?
                                   INT16_MAX : m_pwm0_present_level + APP_LEVEL_STEP_SIZE;
            break;
        }

        /* Initiate node reset */
        case 4:
        {
            /* Clear all the states to reset the node. */
            if (mesh_stack_is_device_provisioned())
            {
#if MESH_FEATURE_GATT_PROXY_ENABLED
                (void) proxy_stop();
#endif
                mesh_stack_config_clear();
                node_reset();
            }
            else
            {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "The device is unprovisioned. Resetting has no effect.\n");
            }
            break;
        }

        default:
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, m_usage_string);
            break;
    }

    if (button_number == 1 || button_number == 2)
    {
        pwm_utils_level_set(&m_pwm, m_pwm0_present_level);
        uint32_t status = app_level_current_value_publish(&m_level_server_0);
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "level: %d\n", m_pwm0_present_level);
        if ( status != NRF_SUCCESS)
        {
            __LOG(LOG_SRC_APP, LOG_LEVEL_WARN, "Unable to publish status message, status: %d\n", status);
        }
    }
}

static void app_rtt_input_handler(int key)
{
    if (key >= '1' && key <= '4')
    {
        uint32_t button_number = key - '1';
        button_event_handler(button_number);
    }
    else
    {
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, m_usage_string);
    }
}

static void unicast_address_print(void)
{
    dsm_local_unicast_address_t node_address;
    dsm_local_unicast_addresses_get(&node_address);
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Node Address: 0x%04x \n", node_address.address_start);
}

static void provisioning_complete_cb(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Successfully provisioned\n");

#if MESH_FEATURE_GATT_ENABLED
    /* Restores the application parameters after switching from the Provisioning
     * service to the Proxy  */
    gap_params_init();
    conn_params_init();
#endif

    unicast_address_print();

//    hal_led_blink_stop();
//    hal_led_mask_set(LEDS_MASK, LED_MASK_STATE_OFF);
//    hal_led_blink_ms(LEDS_MASK, LED_BLINK_INTERVAL_MS, LED_BLINK_CNT_PROV);
}

static void app_mesh_core_event_cb(const nrf_mesh_evt_t * p_evt)
{
    /* USER_NOTE: User can insert mesh core event processing here */
    switch (p_evt->type)
    {
        /* Start user application specific functions only when stack is enabled */
        case NRF_MESH_EVT_ENABLED:
            __LOG(LOG_SRC_APP, LOG_LEVEL_DBG1, "Mesh evt: NRF_MESH_EVT_ENABLED \n");
            {
                static bool s_app_started;
                if (!s_app_started)
                {
                    /* Flash operation initiated during initialization has been completed */
                    app_start();
                    s_app_started = true;
                }
            }
            break;

        default:
            break;
    }
}

static void models_init_cb(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Initializing and adding models\n");
    app_model_init();

}

static void device_identification_start_cb(uint8_t attention_duration_s)
{

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "device_identification_start_cb\n");

//    hal_led_mask_set(LEDS_MASK, false);
//    hal_led_blink_ms(BSP_LED_2_MASK  | BSP_LED_3_MASK,
//                     LED_BLINK_ATTENTION_INTERVAL_MS,
//                     LED_BLINK_ATTENTION_COUNT(attention_duration_s));
}

static void provisioning_aborted_cb(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "provisioning_aborted_cb\n");
}

static void mesh_init(void)
{
    mesh_stack_init_params_t init_params =
    {
#if APP_SCHEDULER_ENABLED
        .core.irq_priority       = NRF_MESH_IRQ_PRIORITY_THREAD,
#else 
        .core.irq_priority       = NRF_MESH_IRQ_PRIORITY_LOWEST,
#endif
        .core.lfclksrc           = DEV_BOARD_LF_CLK_CFG,
        .core.p_uuid             = NULL,
        .models.models_init_cb   = models_init_cb,
        .models.config_server_cb = config_server_evt_cb
    };

    uint32_t status = mesh_stack_init(&init_params, &m_device_provisioned);
    switch (status)
    {
        case NRF_ERROR_INVALID_DATA:
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Data in the persistent memory was corrupted. Device starts as unprovisioned.\n");
			__LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Reset device before start provisioning.\n");
            break;
        case NRF_SUCCESS:
            break;
        default:
            ERROR_CHECK(status);
    }

    /* Register event handler to receive NRF_MESH_EVT_FLASH_STABLE. Application functionality will
    be started after this event */
    nrf_mesh_evt_handler_add(&m_mesh_core_event_handler);
}

static void initialize(void)
{
    __LOG_INIT(LOG_SRC_APP | LOG_SRC_ACCESS | LOG_SRC_BEARER, LOG_LEVEL_INFO, LOG_CALLBACK_DEFAULT);
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "----- BLE Mesh Dimming Server Demo -----\n");

    pwm_utils_init(&m_pwm);

    ERROR_CHECK(app_timer_init());

    ble_stack_init();

#if MESH_FEATURE_GATT_ENABLED
    gap_params_init();
    conn_params_init();
#endif

    mesh_init();

    enocean_translator_init(app_enocean_cb);

}

static void app_start(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Starting application \n");

    /* Enable reception of EnOcean specific AD types from the scanner and install rx callback to
     * intercept incoming ADV packets so that they can be passed to the EnOcean packet processor. */
    bearer_adtype_add(BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA);
    nrf_mesh_rx_cb_set(rx_callback);
}

static void start(void)
{
    rtt_input_enable(app_rtt_input_handler, RTT_INPUT_POLL_PERIOD_MS);

    if (!m_device_provisioned)
    {
        static const uint8_t static_auth_data[NRF_MESH_KEY_SIZE] = STATIC_AUTH_DATA;
        mesh_provisionee_start_params_t prov_start_params =
        {
            .p_static_data    = static_auth_data,
            .prov_sd_ble_opt_set_cb = NULL,
            .prov_complete_cb = provisioning_complete_cb,
            .prov_device_identification_start_cb = device_identification_start_cb,
            .prov_device_identification_stop_cb = NULL,
            .prov_abort_cb = provisioning_aborted_cb,
            .p_device_uri = EX_URI_DM_SERVER
        };
        ERROR_CHECK(mesh_provisionee_prov_start(&prov_start_params));
    }
    else
    {
        unicast_address_print();
    }

    mesh_app_uuid_print(nrf_mesh_configure_device_uuid_get());

    ERROR_CHECK(mesh_stack_start());

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, m_usage_string);
}

static void scheduled_event_handler(void * p_event_data, uint16_t event_size) {
    uint8_t *event = (uint8_t *) p_event_data;
    uint8_t event_id = *event;
    if (event_id == 1) {
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "start handle event_id %d\n", event_id);
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "level: %d\n", m_pwm0_present_level);
        uint32_t status = app_level_current_value_publish(&m_level_server_0);
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "publish status: %d\n", status);
    }

}

static void schedule_event_with_id(uint8_t event_id) {
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "event_id %d\n", event_id);
    app_sched_event_put((void *)&event_id, sizeof(event_id), scheduled_event_handler);
}

int main(void)
{

    APP_SCHED_INIT(APP_SCHED_EVENT_SIZE, APP_SCHED_QUEUE_SIZE);

    initialize();
    start();

    led_state_set(true, false);
    led_level_set(0);

    int counter = 0;
    for (;;)
    {

        app_sched_execute();

        bool done = nrf_mesh_process();
        if (done) {
            (void)sd_app_evt_wait();
        }


        //(void)sd_app_evt_wait();

        check_switch_dimming_state();

        counter++;
        if (counter == 500) {
            schedule_event_with_id(1);
        }

    }
}

// ios test app https://betas.to/Cyg37dFc
// https://bitbucket.org/mixaillmuravev/mesh4.1