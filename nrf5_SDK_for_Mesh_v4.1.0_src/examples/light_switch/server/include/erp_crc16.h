#ifndef __ERP_CRC16_H
#define __ERP_CRC16_H

unsigned short erp_crc16(const unsigned char* data_p, unsigned char length);

#endif /*__ERP_CRC16_H*/