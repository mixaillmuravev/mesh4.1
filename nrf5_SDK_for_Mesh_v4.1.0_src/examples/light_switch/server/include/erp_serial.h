#ifndef __ERP_SERIAL_H
#define __ERP_SERIAL_H

#include <stdint.h>

typedef void (*erp_serial_rx_cb_t)(uint8_t c);

void erp_serial_init(erp_serial_rx_cb_t byte_rx_cb);
void erp_serial_send(unsigned char * buffer, int size);
void erp_serial_process();
void erp_send_level_over_uart(uint16_t value);
void erp_send_on_off_over_uart(uint8_t value);
int erp_serial_receive(unsigned char * buffer,int size);

#endif /*__ERP_SERIAL_H*/