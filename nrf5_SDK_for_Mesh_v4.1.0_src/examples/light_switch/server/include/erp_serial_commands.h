#ifndef ERP_SERIAL_COMMANDS_H__
#define ERP_SERIAL_COMMANDS_H__

#include <stdint.h>
#include "stdbool.h"

// Command Data Type
typedef struct {
    uint8_t length;
    uint8_t command[2];
    uint8_t data[120];
    uint8_t crc16[2];
} erp_serial_command_t;

// OUTBOUND COMMANDS QUEUE STATE
typedef enum {
    ERP_COMMAND_QUEUE_IDLE,
    ERP_COMMAND_QUEUE_WAITING_ACK
} erp_serial_commands_queue_state_t;

// COMMAND RX STATE MACHINE STATES
typedef enum
{
    ERP_COMM_RX_ST_SEQ_BYTE_1,
    ERP_COMM_RX_ST_SEQ_BYTE_2,
    ERP_COMM_RX_LENGTH,
    ERP_COMM_RX_COMMAND_BYTE_1,
    ERP_COMM_RX_COMMAND_BYTE_2,
    ERP_COMM_RX_DATA,
    ERP_COMM_RX_CRC16_BYTE_1,
    ERP_COMM_RX_CRC16_BYTE_2
} erp_serial_command_rx_state_t;

// ERP COMMAND CODES
#define ERP_COMMAND_ACK             0x0000
#define ERP_COMMAND_FACTORY_RESET   0x31cd
#define ERP_COMMAND_LIGHT_POWERUP   0xB9F0
#define ERP_COMMAND_LIGHT_ON_OFF    0xE5D5
#define ERP_COMMAND_LIGHT_LEVEL     0x94C2

#define ERP_PAYLOAD_FACTORY_RESET   0x8000

// ERP COMMAND FIELDS LENGTH
#define ERP_COMMAND_ST_SEQ_LENGTH       2
#define ERP_COMMAND_LENGTH_LENGTH       1
#define ERP_COMMAND_COMM_CODE_LENGTH    2
#define ERP_COMMAND_DATA_MAX_LENGTH     120
#define ERP_COMMAND_CRC16_LENGTH        2

#define ERP_EMPTY_COMMAND_LENGTH        ERP_COMMAND_ST_SEQ_LENGTH+\
                                        ERP_COMMAND_LENGTH_LENGTH+\
                                        ERP_COMMAND_COMM_CODE_LENGTH+\
                                        ERP_COMMAND_CRC16_LENGTH

// Payload is a command without start sequence and CRC16
#define ERP_EMPTY_COMMAND_PAYLOAD_LENGTH    ERP_COMMAND_LENGTH_LENGTH+\
                                            ERP_COMMAND_COMM_CODE_LENGTH

#define ERP_COMMAND_MAX_LENGTH          ERP_EMPTY_COMMAND_LENGTH+\
                                        ERP_COMMAND_DATA_MAX_LENGTH


// Callback for ERP serial commands received event
typedef void (*erp_serial_command_rx_cb_t)(erp_serial_command_t command);

// Callback for ERP serial status
typedef void (*erp_serial_status_cb_t)(bool connected);

void erp_serial_commands_init(erp_serial_command_rx_cb_t command_rx_cb, erp_serial_status_cb_t status_cb);
void erp_serial_commands_process();
void erp_send_command(erp_serial_command_t command);
void erp_send_on_off_command(uint8_t state);
void erp_send_level_command(uint16_t level);
void erp_send_ack_command(uint8_t ack);
void erp_send_powerup_command(uint8_t powerup);
void erp_command_byte_received(uint8_t byte);

#endif /*ERP_SERIAL_COMMANDS_H__*/