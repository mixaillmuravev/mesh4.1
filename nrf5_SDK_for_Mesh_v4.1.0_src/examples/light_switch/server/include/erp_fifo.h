#ifndef __ERP_FIFO_H
#define __ERP_FIFO_H

typedef struct {
     char * buf;
     int head;
     int tail;
     int size;
} erp_fifo_t;

void erp_fifo_init(erp_fifo_t * f, char * buf, int size);
int erp_fifo_read(erp_fifo_t * f, void * buf, int nbytes);
int erp_fifo_write(erp_fifo_t * f, const void * buf, int nbytes);
int erp_fifo_is_empty(erp_fifo_t * f);

#endif /*__ERP_FIFO_H*/