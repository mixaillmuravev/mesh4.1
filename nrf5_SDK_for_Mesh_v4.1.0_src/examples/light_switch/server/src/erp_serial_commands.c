#include "erp_serial_commands.h"
#include "erp_serial.h"
#include "erp_crc16.h"
#include "erp_fifo.h"
#include "nrf.h"
#include "timer.h"
#include "app_error.h" 
#include "nrf_mesh_utils.h"

#include "log.h"


// TIMER PREFERENCES

static timestamp_t erp_last_command_timestamp;

#define ERP_SEND_TIMEOUT_DELAY_MS  (200)
#define ERP_SEND_RETRY_COUNT_MAX   (3)

#define MS_TO_US(t) ((t) * 1000)

// OUTBOUND COMMANDS QUEUE
#define     ERP_SERIAL_COMMANDS_QUEUE_SIZE          32
#define     ERP_SERIAL_COMMANDS_QUEUE_BUFFER_SIZE   ERP_SERIAL_COMMANDS_QUEUE_SIZE*sizeof(erp_serial_command_t)

//static erp_fifo_t erp_serial_commands_queue;
static char erp_serial_commands_queue_buffer[ERP_SERIAL_COMMANDS_QUEUE_BUFFER_SIZE];
static erp_serial_commands_queue_state_t erp_serial_commands_queue_state;

static erp_serial_command_t last_command_sent;
static erp_serial_command_t current_cntrl_command;
static erp_serial_command_t current_ack_command;

static uint8_t  erp_retry_count;

// Registered command receive callback
static erp_serial_command_rx_cb_t   command_rx_callback;

// Registered status callback
static erp_serial_status_cb_t   status_callback;

// Command being currently received
static erp_serial_command_t         comm_rx;
// Index of data byte being received within current command
static uint8_t                      comm_rx_data_index;
// Command RX state machine state
static erp_serial_command_rx_state_t comm_rx_state;

void erp_serial_commands_init(erp_serial_command_rx_cb_t command_rx_cb, erp_serial_status_cb_t status_cb) {
    // Init outbound commands queue (buffer  + state machine)
   // erp_fifo_init(&erp_serial_commands_queue, erp_serial_commands_queue_buffer, ERP_SERIAL_COMMANDS_QUEUE_BUFFER_SIZE);
    erp_serial_commands_queue_state = ERP_COMMAND_QUEUE_IDLE;

    // Set command receive callback
    command_rx_callback = command_rx_cb;

    status_callback = status_cb;

    // Init command RX state machine
    comm_rx_state = ERP_COMM_RX_ST_SEQ_BYTE_1;

    // Init Serial hardware
    erp_serial_init(erp_command_byte_received);
}



void erp_command_send_to_serial(erp_serial_command_t command) {
    uint8_t     data_to_send[ERP_COMMAND_MAX_LENGTH];
    uint16_t    data_to_send_length = command.length + ERP_EMPTY_COMMAND_LENGTH;
    uint8_t *   payload = &data_to_send[ERP_COMMAND_ST_SEQ_LENGTH];
    uint16_t    payload_length = command.length+ERP_EMPTY_COMMAND_PAYLOAD_LENGTH;

    // Setting Start Sequence
    data_to_send[0] = 0xA5;
    data_to_send[1] = 0x5A;

    // Forming the payload
    payload[0] = command.length;
    payload[1] = command.command[0];
    payload[2] = command.command[1];

    for (int i=0; i<command.length; i++){
        payload[ERP_EMPTY_COMMAND_PAYLOAD_LENGTH+i] = command.data[i];
    }

    // Calculating CRC16
    uint16_t crc = erp_crc16(payload, payload_length);

    payload[payload_length] = crc & 0xff;
    payload[payload_length+1] = crc >> 8;

    // Sending the command out
    erp_serial_send(data_to_send, data_to_send_length);
}

int erp_command_queue_is_empty(){
    return current_cntrl_command.length == 0 && current_ack_command.length == 0;
    //return erp_fifo_is_empty(&erp_serial_commands_queue);
}

//void erp_command_queue_enqueue_command(erp_serial_command_t command) {
//    erp_fifo_write(&erp_serial_commands_queue,&command,sizeof(command));
//}

erp_serial_command_t erp_command_queue_dequeue_command(){
    erp_serial_command_t command;
    //erp_fifo_read(&erp_serial_commands_queue,&command,sizeof(command));

    if (current_ack_command.length != 0) {
        command = current_ack_command;
        current_ack_command.length = 0;
    } else if (current_cntrl_command.length != 0) {
        command = current_cntrl_command;
        current_cntrl_command.length = 0;
    }

    return command;
}



void erp_send_next_command(){
    erp_retry_count = 0;

    if (!erp_command_queue_is_empty()) {
        // Setting state machine state  in WAITING_ACK
        erp_last_command_timestamp = timer_now();
        erp_serial_commands_queue_state = ERP_COMMAND_QUEUE_WAITING_ACK;
        last_command_sent = erp_command_queue_dequeue_command();
        erp_command_send_to_serial(last_command_sent);
    } else {
    //    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: No more commands found\n");
        erp_serial_commands_queue_state = ERP_COMMAND_QUEUE_IDLE;
    }
}

void erp_retry_send_command(){
    if (erp_retry_count<ERP_SEND_RETRY_COUNT_MAX) {
        erp_retry_count++;
      //  __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: Retrying send command. Attempt %d\n",erp_retry_count);
        erp_last_command_timestamp = timer_now();
        erp_serial_commands_queue_state = ERP_COMMAND_QUEUE_WAITING_ACK;
        erp_command_send_to_serial(last_command_sent);
    } else {
    //    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: Retry attempt count exceeded. Sending the next command\n");
        status_callback(false);
        erp_send_next_command();
    }
}

void erp_serial_commands_process(){
    if (erp_serial_commands_queue_state == ERP_COMMAND_QUEUE_WAITING_ACK) {
        if(timer_now()>erp_last_command_timestamp+MS_TO_US(ERP_SEND_TIMEOUT_DELAY_MS)){
         //   __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: No ACK received. Try to send the command again\n");
            erp_retry_send_command();
        }
    }
}

// Generic command send routine
void erp_send_command(erp_serial_command_t command) {
    //erp_command_queue_enqueue_command(command);

    switch (erp_serial_commands_queue_state) {
        case ERP_COMMAND_QUEUE_IDLE:
            erp_send_next_command();
            break;
        case ERP_COMMAND_QUEUE_WAITING_ACK:
            break;
    }
}

// Specific commands send
void erp_send_on_off_command(uint8_t state) {
    uint16_t command_code = ERP_COMMAND_LIGHT_ON_OFF;

    erp_serial_command_t command;
    command.length = 1;
    command.command[0] = command_code&0xff;
    command.command[1] = command_code>>8;
    command.data[0] = state;

    current_cntrl_command = command;

    erp_send_command(command);
}

void erp_send_level_command(uint16_t level) {
    uint16_t command_code = ERP_COMMAND_LIGHT_LEVEL;

    erp_serial_command_t command;
    command.length = 2;
    command.command[0] = command_code&0xff;
    command.command[1] = command_code>>8;
    command.data[0] = level&0xff;
    command.data[1] = level>>8;

    current_cntrl_command = command;

    erp_send_command(command);
}

void erp_send_ack_command(uint8_t ack) {
    uint16_t command_code = ERP_COMMAND_ACK;

    erp_serial_command_t command;
    command.length = 1;
    command.command[0] = command_code&0xff;
    command.command[1] = command_code>>8;
    command.data[0] = ack;

    current_ack_command = command;

    erp_send_command(command);
}

void erp_send_powerup_command(uint8_t powerup) {
    uint16_t command_code = ERP_COMMAND_LIGHT_POWERUP;

    erp_serial_command_t command;
    command.length = 1;
    command.command[0] = command_code&0xff;
    command.command[1] = command_code>>8;
    command.data[0] = powerup;
    erp_send_command(command);
}


// Verify the command and send out the external callback if the command is valid
static void erp_command_received(erp_serial_command_t command){
    // Verify CRC16
    uint8_t payload[ERP_COMMAND_MAX_LENGTH];

    uint16_t payload_length = ERP_EMPTY_COMMAND_PAYLOAD_LENGTH + command.length;

    payload[0] = command.length;
    payload[1] = command.command[0];
    payload[2] = command.command[1];
    for (int i=0; i<command.length; i++){
        payload[ERP_EMPTY_COMMAND_PAYLOAD_LENGTH+i] = command.data[i];
    }

    uint16_t crc = erp_crc16(payload, payload_length);

    if ((crc&0xff)==command.crc16[0] && (crc>>8)==command.crc16[1]){
        //CRC is valid
        // Check the command code
        uint16_t command_code = command.command[1]<<8 | command.command[0];
        int command_is_valid = 1;
        switch(command_code) {
            case ERP_COMMAND_ACK:
                // ACK/NACK
                if (command.data[0]==0){
                    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: ACK received. Sending the next command\n");
                    erp_send_next_command();
                    status_callback(true);
                } else {
                    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: NACK received. Trying to send the command again\n");
                    erp_retry_send_command();
                }
                break;
            case ERP_COMMAND_FACTORY_RESET:
                // FACTORY RESET
                if (command.data[0]==0x00 && command.data[1]==0x80){
                    command_is_valid= 1;
                } else {
                    command_is_valid= 0;
                }
                break;
            default:
                // INVALID COMMAND
                command_is_valid= 0;
        }

        if (command_is_valid) {
            command_rx_callback (command);
        } else {
            // Invalid command code
            // NACK should be sent here
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: Invalid Command\n");
        }

    } else {
        // Invalid CRC, command is not accepted
        // NACK should be sent here
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "ERP: Invalid CRC16\n");

    }
}

void erp_command_byte_received(uint8_t byte) {

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "erp res %02x \n", byte);

    switch (comm_rx_state) {
        case ERP_COMM_RX_ST_SEQ_BYTE_1:
            if (byte==0xA5){
                comm_rx_state = ERP_COMM_RX_ST_SEQ_BYTE_2;
            }
            break;
        case ERP_COMM_RX_ST_SEQ_BYTE_2:
            if (byte==0x5A){
                comm_rx_state = ERP_COMM_RX_LENGTH;
            } else {
                comm_rx_state = ERP_COMM_RX_ST_SEQ_BYTE_1;
            }
            break;
        case ERP_COMM_RX_LENGTH:
            if (byte>=0 && byte<=120){
                comm_rx.length = byte;
                comm_rx_state = ERP_COMM_RX_COMMAND_BYTE_1;
            } else {
                comm_rx_state = ERP_COMM_RX_ST_SEQ_BYTE_1;
            }
            break;
        case ERP_COMM_RX_COMMAND_BYTE_1:
            comm_rx.command[0] = byte;
            comm_rx_state = ERP_COMM_RX_COMMAND_BYTE_2;
            break;
        case ERP_COMM_RX_COMMAND_BYTE_2:
            comm_rx.command[1] = byte;
            comm_rx_data_index = 0;
            if (comm_rx.length >0){
                comm_rx_state = ERP_COMM_RX_DATA;
            } else {
                comm_rx_state = ERP_COMM_RX_CRC16_BYTE_1;
            }
            break;
        case ERP_COMM_RX_DATA:
            comm_rx.data[comm_rx_data_index] = byte;
            comm_rx_data_index++;
            if (comm_rx_data_index==comm_rx.length){
                comm_rx_state = ERP_COMM_RX_CRC16_BYTE_1;
            }
            break;
        case ERP_COMM_RX_CRC16_BYTE_1:
            comm_rx.crc16[0] = byte;
            comm_rx_state = ERP_COMM_RX_CRC16_BYTE_2;
            break;
        case ERP_COMM_RX_CRC16_BYTE_2:
            comm_rx.crc16[1] = byte;
            comm_rx_state = ERP_COMM_RX_ST_SEQ_BYTE_1;
            // Sending out internal callback with command for further verification
            erp_command_received(comm_rx);
            break;
    }
}

