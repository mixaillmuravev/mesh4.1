#include "erp_serial.h"
#include "erp_serial_uart.h"

#include "erp_fifo.h"
#include "erp_crc16.h"
#include "nrf_mesh_assert.h"

#include "log.h"

typedef enum
{
    SERIAL_STATE_IDLE,
    SERIAL_STATE_TRANSMIT
} erp_serial_state_t;

static erp_serial_state_t erp_serial_state = SERIAL_STATE_IDLE;

#define ERP_SERIAL_FIFO_BUFFER_SIZE 64

static erp_fifo_t erp_serial_input_fifo;
static erp_fifo_t erp_serial_output_fifo;

static char erp_serial_input_buffer[ERP_SERIAL_FIFO_BUFFER_SIZE];
static char erp_serial_output_buffer[ERP_SERIAL_FIFO_BUFFER_SIZE];

static erp_serial_rx_cb_t erp_serial_byte_rx_cb;
static void erp_char_rx(uint8_t c)
{
    erp_serial_byte_rx_cb(c);
    erp_fifo_write(&erp_serial_input_fifo, &c, 1);
}

static void erp_char_tx(void)
{
    /* Unexpected event */
    NRF_MESH_ASSERT(erp_serial_state != SERIAL_STATE_IDLE);

    if (erp_fifo_is_empty(&erp_serial_output_fifo))
    {
        /* We have nothing to send. */
        erp_serial_uart_tx_stop();
        erp_serial_state = SERIAL_STATE_IDLE;
    }
    else
    {
        uint8_t value;
        erp_fifo_read(&erp_serial_output_fifo, &value, 1);
        erp_serial_uart_byte_send(value);
    }
}


void erp_serial_init(erp_serial_rx_cb_t byte_rx_cb){

    erp_serial_byte_rx_cb = byte_rx_cb;

    // Init Input and Output FIFO
    erp_fifo_init(&erp_serial_input_fifo, erp_serial_input_buffer, ERP_SERIAL_FIFO_BUFFER_SIZE);
    erp_fifo_init(&erp_serial_output_fifo, erp_serial_output_buffer, ERP_SERIAL_FIFO_BUFFER_SIZE);

    NRF_MESH_ASSERT(NRF_SUCCESS == erp_serial_uart_init(erp_char_rx, erp_char_tx));
    erp_serial_uart_receive_set(true);
}

void erp_serial_send(unsigned char * buffer, int size){

    __LOG_XB(LOG_SRC_APP, LOG_LEVEL_INFO, "erp cmd ", buffer, size);

    erp_fifo_write(&erp_serial_output_fifo,buffer,size);
    if(erp_serial_state==SERIAL_STATE_IDLE){
        erp_serial_state=SERIAL_STATE_TRANSMIT;
        erp_serial_uart_tx_start();
        erp_char_tx();
    }
}

int erp_serial_receive(unsigned char * buffer,int size) {
    int bytes_read = erp_fifo_read(&erp_serial_input_fifo,buffer,size);
    return bytes_read;
}

void erp_serial_process() {
    erp_serial_uart_process();
}

void erp_send_level_over_uart(uint16_t value){
    unsigned char message_with_crc[] = "\xA5\x5A\x02\xC2\x94\x00\x00\xFF\xFF";
    unsigned char message_length = 7;
    unsigned char message_with_crc_length = message_length+2;

    // Setting Level value to the message

    // Little endian 16 bit value pack
    message_with_crc[5] = value & 0xFF;
    message_with_crc[6] = value >> 8;

    // Calculating CRC16
    unsigned short crc = erp_crc16(message_with_crc, message_length);
    // Little endian 16 bit value pack
    message_with_crc[message_length] = crc & 0xff;
    message_with_crc[message_length + 1] = crc >> 8;

    erp_serial_send(message_with_crc, message_with_crc_length);
}

void erp_send_on_off_over_uart(uint8_t value){
    // Default is OFF
    unsigned char message_with_crc[] = "\xA5\x5A\x01\xD6\xE5\x00\xFF\xFF";
    unsigned char message_length = 6;
    unsigned char message_with_crc_length = message_length+2;

    // Setting value to the message
    if (value) {
        // Change to ON
        message_with_crc[5] = '\x01';
    }

    // Calculating CRC16
    unsigned short crc = erp_crc16(message_with_crc, message_length);
    // Little endian 16 bit value pack
    message_with_crc[message_length] = crc & 0xff;
    message_with_crc[message_length + 1] = crc >> 8;

    erp_serial_send(message_with_crc, message_with_crc_length);
}